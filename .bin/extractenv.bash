#!/bin/bash
before=`env`
# echo $before
case $1 in
  "only-value")
    . $3;
    $4;
    env | grep --color=none "^$2.*=" | sed -r -e 's#^[[:alnum:]_]+=##g'
    ;;
  "bash")
    . $3;
    $4;
    env | grep --color=none "^$2.*="
    ;;
  "fish")
    echo "fish"
    . $3;
    $4;
    env | grep --color=none -e "^$2.*=" | sed -r -e 's_^([[:alnum:]_]+)=(.*)$_set \1 "\2"_g'
    ;;

#  *)
#    . $2;
#    $3;
#    env | grep --color=none "^$1.*=" | sed -r -e 's#^([[:alnum:]_]+)=#set \1 "#g'; echo -n '"' ''
#    ;;
esac
