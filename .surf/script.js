// ==UserScript==
// @name         Instant Move
// @namespace    http://cryks.hateblo.jp/
// @description  Enable move button on Instant Upload album.
// @match        https://picasaweb.google.com/lh/reorder*
// ==/UserScript==
 
(function() {
    var move = document.getElementById('lhid_move');
    var enableButton = document.createElement('button');
    enableButton.innerText = 'Przenieś';
    enableButton.onclick = function() {
        move.disabled = false;
        return false;
    };
    move.parentNode.appendChild(enableButton);
})();

// https://picasaweb.google.com/lh/reorder?uname=113392522579727309312&aid=6131649765084939153
