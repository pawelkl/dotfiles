#!/usr/bin/fish
# set -l du (df / | tail -n1 | sed "s/ */ /g" | cut -d' ' -f 5 | cut -d'%' -f1)

# set -l du (df / | grep -Eo '([0-9]{1,3})%')
# if test $du -gt 80
#   error du $du%%
# end

function spaceused
  set target $argv
  if [ -z $target ]
    set target '/'
  end
  # df $argv | grep --color=never -Eo '([0-9]{1,3})%'
  df $target | grep --color=never --extended-regexp --only-matching '([0-9]{1,3})%'
  # df $argv | sed -En 's/.*[[:space:]]([0-9]{1,3})%.*/\1/p'
end
