function _select_network
	netctl list | sed 's/^[*] /-/' | dmenu | sed -r 's/^[-]?[ ]*//'
end
