#!/usr/bin/fish
function aroot
  cd $argv
  sudo mount -t proc proc proc/
  sudo mount --rbind /sys sys/
  sudo mount --rbind /dev dev/
  sudo chroot . /bin/bash
end
