function fish_right_prompt
	# prompt_segment blue black (right_segment_separator)
  # powerline shell right --last_exit_code=$status
  # prompt_right_segment yellow black (nfiles) # files+folders

  # prompt_right_segment yellow black (echo -e (count (lsdirs))"d/"(count (lsfiles)))
  prompt_right_segment blue black (count (lsdirs))"d"

  prompt_right_segment "#ae8e00" black (count (lsfiles))"f"
  prompt_right_segment blue black (tbytes)
  prompt_right_segment yellow black (fbytes)
  # prompt_finish
  prompt_finish_right
end
