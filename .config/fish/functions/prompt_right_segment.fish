function prompt_right_segment --description 'Function to draw a segment'
	set -l bg
  set -l fg
  set argvn (count $argv)
  if [ $argvn -gt 0 ]
    set bg $argv[1]
  else
    set bg normal
  end
  if [ $argvn -gt 1 ]
    set fg $argv[2]
  else
    set fg normal
  end
  if [ "$argv[1]" != "$current_bg" ]
    # set_color -b $bg
    set_color $bg
    echo -n "$right_segment_separator"
    set_color -b $bg
    # set_color -b $bg
    set_color $fg
    # echo -n "$right_segment_separator "
  else
    set_color -b $bg
    set_color $fg
    # echo -n " "
  end
  set current_bg $argv[1]
  if [ $argvn -gt 2 ]
    echo -n -s " " $argv[3] " "
  end
end
