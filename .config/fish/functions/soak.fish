#!/usr/bin/fish
function soak
  function error
    echo -n "$0"
    if test -n "$argv"
      echo -e "$argv"
    else
      echo "erroneus args"
    # exit
    end
  end
  switch (count $argv)
    case 0;error;
    case 1;error;
    case 2;
      set exec $argv[1..-2]
      set file $argv[-1]
      set ex "$exec $file | sponge $file"
    case '*';
      if test $argv[-1] = "debug"
        set exec $argv[1..-3]
        set file $argv[-2]
        set ex "$exec $file | sponge $file"
      end
      echo "exe: $exec"
      echo "file: $file"
      echo "cmd: $ex"
  end
  if test -f $file
    eval $ex
  else
    error "bad file"
  end
end
