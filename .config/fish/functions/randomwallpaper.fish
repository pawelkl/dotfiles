#!/bin/env fish
function randomwallpaper
  function dl
     feh --bg-fill --no-fehbg (curl -s (rand $argv) | psub)
  end
  function rand
    curl -s 'http://www.reddit.com/r'$argv[1] | jshon -e data -e children -a -e data -e url -u | shuf -n1
  end
  set many (count $argv)
  switch $many
    case 0;
      echo Args $many
    case 1;
      echo Args $many
      set sub   $argv[1]
    case 2;
      echo Args $many
      set sub   $argv[1]
      set top   $argv[2]
    case 3;
      echo Args $many
      set sub   $argv[1]
      set top   $argv[2]
      set limit $argv[3]
    case *;
      echo Args $many
  end

  # /$sub(/$top).json(?limit=$limit)
  if test -n "$top"
    set top   "/$top";\
  else
    set top earthporn
  end

  if test -n "$limit"
    set limit "?limit=$limit"
  end
  set dl (printf "/%s%s.json%s" "$sub" "$top" "$limit")
  # rand $dl
  dl $dl
end

# randomwallpaper $argv
