function fun
	wget randomfunfacts.com -O - 2>/dev/null |\
    grep \<strong\> | sed 's;^.*<i>\(.*\)</i>.*$;\1;' |\
    while read FUNFACT;\
      notify-send -i gtk-dialog-info "RandomFunFact" "$FUNFACT";\
    end
end
