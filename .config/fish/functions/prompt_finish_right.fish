function prompt_finish_right --description 'Close open segments'
	if [ -n $current_bg ]
    set_color -b normal
    set_color $current_bg
  end
  set -g current_bg NONE
end
