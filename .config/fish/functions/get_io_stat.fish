function get_io_stat
	iostat -d /dev/$argv | grep --color=never "^$argv" | awk '{print $2 " " $3}' | read Kread Kwrite; echo (echo "scale=0;$Kread/1" | bc; echo "scale=0;$Kwrite/1" | bc)
end
