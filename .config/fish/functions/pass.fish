#!/usr/bin/fish
function pass
  switch (count $argv)
    case 0;echo error;
    case 2;
      cat $1 | pv -pterab --size (du -sb "$1" | awk '{print $1}') | tee $2
    case '*';
end
 # zenity --progress --auto-kill --text='Progress of moving $1 to $2' --auto-close --pulsate
