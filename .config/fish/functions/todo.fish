#!/usr/bin/fish
function todo
  set file ~/TODO
  function error;echo "erroneus args";end
  switch (count $argv)
    case 0;error;
    case 1;
      switch $argv[1]
        case ls;cat $file;
        case '*';error;
      end;
    case '*';
      switch $argv[1]
        case add;
            echo "* $argv[2..-1]" >> $file;
        case addat;
            set match (grep -m1 --color=none "$argv[2]" $file)
            set added "$argv[3..-1]"
            set prepend (echo "  " | sed 's# #\\\ #g')
            set checkmark (echo "[ ]")
            sed "/$match/a\ \ $checkmark $added" $file | sponge $file
            #sponge/soak
        case check;
            set match (grep -m1 --color=none "$argv[2]" $file)
            set to (echo $match | sed "s#\[ \]#[X]#")
            echo -e "-\"$match\"\n+\"$to\""
            sed "s/$match/$to/g" $file | sponge $file
        # case rm;
        # case search;
        case '*';error;
      end;
  end
end
