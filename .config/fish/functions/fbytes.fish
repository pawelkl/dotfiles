function fbytes --description 'free space in current directory'
	df -Ph . | awk 'NR==2 {print $4}'
end
