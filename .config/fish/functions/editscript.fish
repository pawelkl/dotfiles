#!/usr/bin/fish
function editscript
  if [ (count $argv) -lt 2 ]
    set editwith $EDITOR
  else
    set editwith $argv[2]
  end
  eval "$editwith (which $argv[1])"
end
