function tbytes --description 'calculates the total size of the files in the current directory'
	# set -l tBytes (ls -al | grep "^-" | awk 'BEGIN {i=0} { i += $5 } END { print i }')
  set -l tBytes (ls -al | awk 'BEGIN {i=0} /^-/ { i += $5 } END { print i }')

  if test $tBytes -lt 1048576
    set -g total (echo "scale=0; $tBytes/1048" | bc)
    set -g units "K"
  else
    set -g total (echo "scale=0; $tBytes/1048576" | bc)
    set -g units "M"
  end
  echo -n "$total$units"
end
