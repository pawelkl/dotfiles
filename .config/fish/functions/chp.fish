#!/usr/bin/fish
function chp
  set CHROME_EXEC "google-chrome-unstable"
  set CHROME_ARGS "--disable-new-menu-style"
  set CHROME_PROFILES_ROOT "$HOME/chrome/"
  set CHROME_PROFILE "p.empty1"
  set step 1

  # function dmenu
  #   if [ -f $HOME/.dmenurc ]
  #     set DMENU (extractenv.bash only-value DMENU $HOME/.dmenurc)
  #   else
  #     set DMENU 'dmenu -i'
  #   end
  #   eval $DMENU $argv
  # end

  # function printstep
  #   echo -n "   $step. $argv"
  #   set step (echo "$step + 1" | bc)
  # end

  # function printdetails
  #   echo -ne "\n      [$argv]"
  # end

  # function printcr
  #   echo -e "\r$argv"
  # end

  # function printerror
  #   echo -n "[$argv]"
  #   printcr "ERR";
  # end

  # function profile_select
  #   # find $argv -maxdepth 1 -type d -name 'p.*' | sed 's#^./##' | dmenu
  #   set chosen (find ~/chrome/ -maxdepth 1 -type d -name 'p.*' | sed 's#^.*/p.##' | dmenu)
  #   echo "p.$chosen"
  # end

  # function printappend
  #   echo -n "[$argv]"
  # end

  # function execute_profile

  #   set datadir_arg "--user-data-dir="
  #   set profile_arg "--profile-directory="
  #   set app_arg "--app="
  #   set incognito_arg "--incognito"

  #   switch (count $argv)
  #     case 0;
  #       printerror "wrong number of arguments (0) for chrome executing";return;
  #     case 1;
  #       printappend "1 arg";
  #       set dest $argv[1]
  #       printerror "nohup $CHROME_EXEC $CHROME_ARGS (echo $datadir_arg$CHROME_PROFILES_ROOT $profile_arg$dest) >/dev/null ^/dev/null &;"
  #       nohup $CHROME_EXEC $CHROME_ARGS \
  #         (echo $datadir_arg$CHROME_PROFILES_ROOT $profile_arg$dest) \
  #         >/dev/null ^/dev/null &;
  #     # case 2;
  #     #   printappend "2 args";
  #     #   chrome $datadir_arg"$CHROME_PROFILES_ROOT" $profile_arg"$argv[3]";
  #     # case 3;
  #     #   printappend "3 args";
  #     #   chrome $datadir_arg"$CHROME_PROFILES_ROOT" $profile_arg"$argv[3]" $argv[3];
  #     case *;
  #       printerror "wrong number of arguments (count $argv) for chrome executing";return;
  #   end
  #   # chrome \
  #   # --user-data-dir="$1" \
  #   # --profile-directory="$2" $3
  # end

  # function check_arguments
  #   printstep 1 "check arguments"
  #   switch (count $argv)
  #     # case 0; printerror "no arguments"; return;
  #     case 0; execute_profile $CHROME_PROFILE; return;
  #     case 1; printappend 1; # set ok ok;
  #     case 2; printappend 2; # set ok ok;
  #     case *; return;
  #   end
  #   printcr "OK"
  # end

  # function check_directory
  #   printstep 2 "check directory"
  #   if [ -z $CHROME_PROFILES_ROOT ];
  #      and [ -d $CHROME_PROFILES_ROOT ]
  #     if [ -d $HOME/chrome ]
  #       set DIR $HOME/chrome
  #       cd $CHROME_PROFILES_ROOT
  #     else
  #       return
  #     end
  #   else
  #     cd $CHROME_PROFILES_ROOT
  #   end
  #   printcr "OK"
  # end

  # function parse_arguments
  #   printstep 4 "parse first argument"
  #   switch $argv[1]
  #     case "profile";
  #       if [ (count $argv) -gt 1 ]; and [ -d $argv[2] ]
  #         printappend "profile"
  #         printappend "$argv[2]"
  #         execute_profile $argv[2]
  #       end
  #     case "list";
  #       set sel (eval profile_select $CHROME_PROFILES_ROOT)
  #       printdetails "list"
  #       printappend "$sel"
  #       execute_profile $sel
  #     case "app";
  #       set app $argv[2]
  #     case "apps";
  #       set
  #       set app $argv[2]
  #     case "hangouts";
  #     case "incognito";
  #     case "music";
  #     case "security";
  #     case *;printerror "none chosen!";
  #   end
  #   printcr "OK"
  # end

  # function check_run
  #   sleep 3
  #   set lol (ps -eo pid,user,args --sort user,pid -u pawel | grep chrome)
  #   echo (echo $lol | grep profile)
  # end

  # if test (count $argv) -eq 0
    nohup $CHROME_EXEC --user-data-dir=$CHROME_PROFILES_ROOT --profile-directory="$CHROME_PROFILE" "$CHROME_ARGS"
  # else
  #   check_arguments $argv
  #   check_directory $argv
  #   parse_arguments $argv
  # end
  # check_run
  # printstep "script finished"
end
