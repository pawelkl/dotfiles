function search

  if [ (count $argv) -gt 1 ]
    pacsearch "$argv[1]" | grep -A1 "$argv[2..-1]" | grep -Ev '^--$'
    # echo 'pacsearch $argv[1] | grep -B1 $argv[2..-1] | grep -Ev ^--$'
  	aura -As "$argv[1]" | grep -A1 "$argv[2..-1] " | grep -Ev '^--$'
  else
    pacsearch "$argv"
    # aura -Ss $argv
    aura -As "$argv"
  end
end
