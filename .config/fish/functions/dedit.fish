function dedit
	if test (count $argv) -lt 1
set result "~/.config/systemd/user/"(echo (dlist) | to_array | sed 's_.*/systemd/user/__' | dmenu)
else
set result (find ~/.config/systemd/user/ -iname "$argv*" )
end
echo $result
nano (echo $result | to_array | head -n1)
end
