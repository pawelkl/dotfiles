function ntest --description 'Run command(3) N times(1) and write to output(2)'
	for i in (seq 1 $1); time -f '%E' -a -o $2 "$3";end
end
