function ls
	command ls --time-style=long-iso --color=auto $argv
end
