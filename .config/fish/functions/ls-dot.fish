function ls-dot
	ls -lA | grep -v '^d' | grep -E ':[0-9]{2} [.]' | less
end
