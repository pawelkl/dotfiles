function getopenfiles
	file (file /proc/(pidof $argv)/fd/* | grep -v -e broken -e /dev/ | sed 's/.* to //')
end
