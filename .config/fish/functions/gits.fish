function gits
	script -qc 'git status -s' \
 | sed -r -e 's/^[[:space:]]?([^a-zA-Z? \d128\d255]{4}m)[[:space:]]*([AM?]{1,2})[[:space:]]*([^a-zA-Z? \d128\d255]{2}m[ ]{1,2})/\3 \2 \1/' -e 's/^[[:space:]]*//' -e 's/^.{4}//' -e 's/ [?]{2} /?  /'
end
