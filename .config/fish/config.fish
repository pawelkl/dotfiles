# Path to your oh-my-fish.
set fish_mainpath $HOME/.config/fish
set fish_path     $HOME/.oh-my-fish
set TERM          rxvt-unicode
set EDITOR        subl

set -ug GREP_OPTIONS

function addpath
  set PATH $argv $PATH
end
addpath ~/.bin

# source $fish_mainpath/themes/bobthefish_mod/fish_title.fish
# source $fish_mainpath/themes/bobthefish_mod/fish_prompt.fish
# source $fish_mainpath/functions/fish_right_prompt.fish

if test -e ~/.dir_colors;
  eval (dircolors -c ~/.dir_colors | sed 's#setenv #set -U #')
else
  eval (dircolors -c | sed 's#setenv #set -U #')
end


# Theme
# set fish_theme robbyrussell
# set fish_theme syl20bnr
# set fish_theme jacaetevha
# set fish_theme agnoster
# set fish_theme bobthefish
# set fish_theme agnoster-MOD

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-fish/plugins/*)
# Custom plugins may be added to ~/.oh-my-fish/custom/plugins/
# Example format: set fish_plugins autojump bundler

# Path to your custom folder (default path is $FISH/custom)
# set fish_custom $HOME/dotfiles/oh-my-fish

      # source $fish_mainpath/custom/other.fish
      # source $fish_path/oh-my-fish.fish
      # source /usr/share/fry/fry.fish

      # set fish_plugins rvm git rails bundler

# set DESKTOP_SESSION gnome

# source $fish_mainpath/custom/powerline.fish
# source $fish_mainpath/custom/powerline-left.fish

# source $fish_mainpath/custom/powerline-right.fish

# disabled git

# source $fish_mainpath/themes/bobthefish_mod/fish_rightprompt.fish
# # source $fish_mainpath/custom/powerline-native.fish

source $fish_mainpath/powerline.fish
