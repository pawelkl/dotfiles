#!/bin/bash
#!/usr/bin/fish
# This script is a skeleton for callback script to use with tpb.
# tpb can call a program on each button press and state change. It passes an
# identifier as first argument and the new state as second argument to the
# callback. So you can do fancy things :)
# Supported identifiers and states are:
#
# IDENTIFIER        STATES/VALUE
# thinkpad          pressed
# home              pressed
# search            pressed
# mail              pressed
# favorites         pressed
# reload            pressed
# abort             pressed
# backward          pressed
# forward           pressed
# wireless          pressed
# fn                pressed
# zoom              on, off
# thinklight        on, off
# display           lcd, crt, both
# expand            on, off
# brightness        PERCENT
# volume            PERCENT
# mute              on, off
# ac_power          connected, disconnected
# powermgt_ac       high, auto, manual
# powermgt_battery  high, auto, manual

osd () {
  pnotify "tpb" "$1" "$2"
  # notify-send "$1" "$2" -t 3 -h string:value:$3 -h string:synchronous:volume
}

case $1 in
  (thinkpad)
    osd "resumed from suspend" "suspend triggered by thinkvantage button"
    echo "CALLBACK: $0 $1 $2 (should be thinkpad pressed)"
    ;;
  (home)
    echo "CALLBACK: $0 $1 $2 (should be home pressed)"
    ;;
  (search)
    echo "CALLBACK: $0 $1 $2 (should be search pressed)"
    ;;
  (mail)
    echo "CALLBACK: $0 $1 $2 (should be mail pressed)"
    ;;
  (favorites)
    echo "CALLBACK: $0 $1 $2 (should be favorites pressed)"
    ;;
  (reload)
    echo "CALLBACK: $0 $1 $2 (should be reload pressed)"
    ;;
  (abort)
    echo "CALLBACK: $0 $1 $2 (should be abort pressed)"
    ;;
  (backward)
    osd backlight up
    # bl osd down 15
    echo "CALLBACK: $0 $1 $2 (should be backward pressed)"
    ;;
  (forward)
    osd backlight down
    # bl osd up 15
    echo "CALLBACK: $0 $1 $2 (should be forward pressed)"
    ;;
  (wireless)
    osd wireless $2
    echo "CALLBACK: $0 $1 $2 (should be wireless pressed)"
    ;;
  (fn)
    osd fn pushed
    echo "CALLBACK: $0 $1 $2 (should be fn pressed)"
    ;;
  (zoom)
    case $2 in
      (on)
        osd zoom on
        xzoom
        echo "CALLBACK: $0 $1 $2 (should be zoom on)"
        ;;
      (off)
        osd zoom off
        pkill xzoom
        echo "CALLBACK: $0 $1 $2 (should be zoom off)"
        ;;
    esac
    ;;
  (thinklight)
    case $2 in
      (on)
        osd thinklight on
        echo "CALLBACK: $0 $1 $2 (should be thinklight on)"
        ;;
      (off)
        osd thinklight off
        echo "CALLBACK: $0 $1 $2 (should be thinklight off)"
        ;;
    esac
    ;;
  (display)
    case $2 in
      (lcd)
        osd lcd
        echo "CALLBACK: $0 $1 $2 (should be display lcd)"
        ;;
      (crt)
        osd crt
        echo "CALLBACK: $0 $1 $2 (should be display crt)"
        ;;
      (both)
        osd both
        echo "CALLBACK: $0 $1 $2 (should be display both)"
        ;;
    esac
    ;;
  (expand)
    case $2 in
      (on)
        osd expand on
        echo "CALLBACK: $0 $1 $2 (should be expand on)"
        ;;
      (off)
        osd expand off
        echo "CALLBACK: $0 $1 $2 (should be expand off)"
        ;;
    esac
    ;;
  (brightness)
    osd brightness "$2"
    echo "CALLBACK: $0 $1 $2 (should be brightness PERCENT)"
    ;;
  (volume)
    osd volume $2
    echo "CALLBACK: $0 $1 $2 (should be volume PERCENT)"
    # exec canberra-gtk-play --file=/usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
    ;;
  (mute)
    case $2 in
      (on)
        osd mute on
        echo "CALLBACK: $0 $1 $2 (should be mute on)"
        ;;
      (off)
        osd mute off
        echo "CALLBACK: $0 $1 $2 (should be mute off)"
        ;;
    esac
    ;;
  (ac_power)
    case $2 in
      (connected)
        osd "power cord" connected
        echo "CALLBACK: $0 $1 $2 (should be ac_power connected)"
        ;;
      (disconnected)
        osd "power cord" disconnected
        echo "CALLBACK: $0 $1 $2 (should be ac_power disconnected)"
        ;;
    esac
    ;;
  (powermgt_ac)
    case $2 in
      (high)
        osd "power management" high
        echo "CALLBACK: $0 $1 $2 (should be powermgt_ac high)"
        ;;
      (auto)
        osd "power management" auto
        echo "CALLBACK: $0 $1 $2 (should be powermgt_ac auto)"
        ;;
      (manual)
        osd "power management" manual
        echo "CALLBACK: $0 $1 $2 (should be powermgt_ac manual)"
        ;;
    esac
    ;;
  (powermgt_battery)
    case $2 in
      (high)
        osd "battery management" high
        echo "CALLBACK: $0 $1 $2 (should be powermgt_battery high)"
        ;;
      (auto)
        osd "battery management" auto
        echo "CALLBACK: $0 $1 $2 (should be powermgt_battery auto)"
        ;;
      (manual)
        osd "battery management" manual
        echo "CALLBACK: $0 $1 $2 (should be powermgt_battery manual)"
        ;;
    esac
    ;;
  (*)
    osd "callback" "$0 $1 $2"
    echo "CALLBACK: $0 $1 $2 (TYPE UNKNOWN)"
    ;;
esac
# exec canberra-gtk-play --file=/usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
